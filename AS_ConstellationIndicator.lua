--Credit: Jane in the AS Discord
--License: MIT

-- Hope we remember to update the version code. lol
local VERSION = "1.1.0"

-- The list of all 16 constellation with their names, types (1 = Bright, 2 = Dim, 3 = Faint), colors and fixed moon phases if there are any.
-- Note: due to the limitation on number of usable palettes, some constellations have to share the same palettes.
-- colors.white, colors.black and colors.lime are reserved.
local CONSTELLATIONS = {
	{ name = "Aevitas",		type = 1,	palette = colors.orange },
	{ name = "Armara",		type = 1,	palette = colors.magenta },
	{ name = "Discidia",	type = 1,	palette = colors.lightBlue },
	{ name = "Evorsio",		type = 1,	palette = colors.yellow },
	{ name = "Vicio",		type = 1,	palette = colors.pink },
	
	{ name = "Bootes",		type = 2,	palette = colors.gray },
	{ name = "Fornax",		type = 2,	palette = colors.lightGray },
	{ name = "Horologium",	type = 2,	palette = colors.cyan,		show = {2, 2, 2, 2, 2, 2, 2, 2} },
	{ name = "Lucerna",		type = 2,	palette = colors.purple },
	{ name = "Mineralis",	type = 2,	palette = colors.blue },
	{ name = "Octans",		type = 2,	palette = colors.brown },
	{ name = "Pelotrio",	type = 2,	palette = colors.green,		show = {1, 0, 0, 0, 1, 0, 0, 0} },
	
	{ name = "Alcara",		type = 3,	palette = colors.red },
	{ name = "Gelu",		type = 3,	palette = colors.red },
	{ name = "Ulteria",		type = 3,	palette = colors.red },
	{ name = "Vorux",		type = 3,	palette = colors.red }
}

local numDynamicConstellations = 0
for _, v in ipairs(CONSTELLATIONS) do
	if v.show == nil then
		numDynamicConstellations = numDynamicConstellations + 1
	end
end

local REDNET_PROTOCOLS = {
	BROADCAST_CONSTELLATIONS = "AS_ConstellationIndicator_Broadcast",
	REQUEST_CONSTELLATION = "AS_ConstellationIndicator_Request",
	RESPONSE_CONSTELLATION = "AS_ConstellationIndicator_Response"
}

-- Clears any text from the screen and sets the cursor to the top left of the screen.
local function clearTerminal()
	term.clear()
	term.setCursorPos(1, 1)
end

------------------------------------------------------------------------------
-- Step 1. Handle program arguments.
------------------------------------------------------------------------------

local RESET_TYPE = {
	ALL = "all",
	FIRST_MON = "firstMon",
	SECOND_MON = "secondMon",
	REDNET = "rednet",
	SOLAR_ECLIPSE = "solarEclipse",
	CONSTELLATION_ONE = "oneConstellation",
	CONSTELLATION_BRIGHT = "brightConstellations",
	CONSTELLATION_DIM = "dimConstellations",
	CONSTELLATION_FAINT = "faintConstellations",
	CONSTELLATION_ALL = "allConstellations"
}

local tArgs = {...}
local resetType = nil
local resetConstellationIndex = nil
local initialColorScheme = nil
local initialDetailedConstellationIndex = nil
local canWriteToFile = true
local canRun = true

-- Reads and verifies the program arguments.
-- Returns whether the program should continue.
local function readProgramArgs()
	local readingIdx = 1
	local numArgs = #tArgs
	while numArgs >= readingIdx do
		local arg = tArgs[readingIdx]
		
		if arg == "-r" or arg == "--reset" then
			readingIdx = readingIdx + 1
			arg = tArgs[readingIdx]
			if arg == "all" or arg == "All" then
				resetType = RESET_TYPE.ALL
			elseif arg == "firstMonitor" or arg == "FirstMonitor" or arg == "1m" or arg == "1M" then
				resetType = RESET_TYPE.FIRST_MON
			elseif arg == "secondMonitor" or arg == "SecondMonitor" or arg == "2m" or arg == "2M" then
				resetType = RESET_TYPE.SECOND_MON
			elseif arg == "rednet" or arg == "Rednet" or arg == "rn" or arg == "Rn" then
				resetType = RESET_TYPE.REDNET
			elseif arg == "solarEclipse" or arg == "SolarEclipse" or arg == "se" or arg == "SE" then
				resetType = RESET_TYPE.SOLAR_ECLIPSE
			elseif arg == "constellation" or arg == "con" then
				readingIdx = readingIdx + 1
				arg = tArgs[readingIdx]
				local i = -1
				for idx, value in ipairs(CONSTELLATIONS) do
					if value.name == arg then
						if value.show ~= nil then
							print("Error: Unable to reset the visibilities of " .. value.name .. " because they are fixed.")
							return false
						end
						i = idx
						break
					end
				end
				if i >= 1 then
					resetType = RESET_TYPE.CONSTELLATION_ONE
					resetConstellationIndex = i
				else
					print("Error: Unrecognized constellation name. Please make sure it is a valid name with the first letter in uppercase.")
					return false
				end
			elseif arg == "constellationBright" or arg == "conB" then
				resetType = RESET_TYPE.CONSTELLATION_BRIGHT
			elseif arg == "constellationDim" or arg == "conD" then
				resetType = RESET_TYPE.CONSTELLATION_DIM
			elseif arg == "constellationFaint" or arg == "conF" then
				resetType = RESET_TYPE.CONSTELLATION_FAINT
			elseif arg == "constellationAll" or arg == "conAll" then
				resetType = RESET_TYPE.CONSTELLATION_ALL
			else
				print("Error: Unexpected <type> for reset operation. Expected 'all', 'firstMonitor', 'secondMonitor', 'rednet', 'solarEclipse', 'constellation', 'constellationBright', 'constellationDim', 'constellationFaint' or 'constellationAll'. Run again with -h or --help for more detail.")
				return false
			end
		elseif arg == "-ra" or arg == "--resetAll" then
			resetType = RESET_TYPE.ALL
		elseif arg == "-c" or arg == "--color" then
			readingIdx = readingIdx + 1
			arg = tArgs[readingIdx]
			if arg == '1' then
				initialColorScheme = 1
			elseif arg == '2' then
				initialColorScheme = 2
			elseif arg == '3' then
				initialColorScheme = 3
			else
				print("Error: Unexpected <color-scheme>. Expected '1', '2' or '3'. Run again with -h or --help for more detail.")
				return false
			end
		elseif arg == "-d" or arg == "--detail" then
			readingIdx = readingIdx + 1
			arg = tArgs[readingIdx]
			local i = -1
			for idx, value in ipairs(CONSTELLATIONS) do
				if value.name == arg then
					i = idx
					break
				end
			end
			if i >= 1 then
				initialDetailedConstellationIndex = i
			else
				print("Error: Unrecognized constellation name. Please make sure it is a valid name with the first letter in uppercase.")
				return false
			end
		elseif arg == "-W" or arg == "--dontWrite" then
			canWriteToFile = false
		elseif arg == "-s" or arg == "--stop" then
			canRun = false
		elseif arg == "-v" or arg == "--version" then
			print("Version " .. VERSION)
			return false
		elseif arg == "-h" or arg == "--help" then
			clearTerminal()
			term.setTextColor(colors.yellow)
			print("Astral Sorcery - Constellation Indicator v" .. VERSION)
			term.setTextColor(colors.white)
			print()
			print("  This program will use the first monitor (3 width x 3 height) to display the moon phases on the following 8 days and indicate which constellations will be visible on these days.")
			print("  It will also use the second monitor (6 width x 3 height) to display two day numbers when a specific constellation will be visible on. The constellation can be cycled through by right-clicking the monitor with empty hands.")
			print("  Finally, it will send Rednet messages about which constellations are visible every night. You can utilize it by using another set of computers to run AS_ConstellationRedstone to emit redstone signals accordingly.")
			print()
			print("Press enter to continue")
			read()
			
			clearTerminal()
			print("Supported arguments (1/5):")
			print("  -r <type>, --reset <type>")
			print("    Resets the value of a specific data before actually running the program. <type> can be any of the following:")
			print("    - 'all' or 'All' for resetting all data.")
			print("    - '1m', '1M', 'firstMonitor' or 'FirstMonitor' for resetting the side connecting to the first monitor.")
			print("    - '2m', '2M', 'secondMonitor' or 'SecondMonitor' for resetting the side connecting to the second monitor.")
			print("    - 'rn', 'Rn', 'rednet' or 'Rednet' for resetting the side connecting to the modem for sending Rednet messages.")
			print()
			print("Press enter to continue")
			read()
			
			clearTerminal()
			print("Supported arguments (2/5):")
			print("    - 'se', 'SE', 'solarEclipse' or 'SolarEclipse' for resetting the day when solar eclipse happened.")
			print("    - 'con <Constellation-Name>' or 'constellation <Constellation-Name>' for resetting the moon phases of such constellation.")
			print("    - 'conB' or 'constellationBright' for resetting the moon phases of all bright constellations.")
			print("    - 'conD' or 'constellationDim' for resetting the moon phases of all dim constellations.")
			print("    - 'conF' or 'constellationFaint' for resetting the moon phases of all faint constellations.")
			print("    - 'conAll' or 'constellationAll' for resetting the moon phases of all constellations.")
			print("    Note: use -r only once or the last one will override the previous ones.")
			print()
			print("Press enter to continue")
			read()
			
			clearTerminal()
			print("Supported arguments (3/5):")
			print("  -ra, --resetAll")
			print("    Same as '-r all'")
			print()
			print("  -c <color-scheme>, --color <color-scheme>")
			print("    Sets the initial color scheme for the constellation names on the first monitor. <color-scheme> can be either one of the following:")
			print("    - '1' for all names in white color.")
			print("    - '2' for names colored by the constellation types.")
			print("    - '3' for names colored by each constellation. But due to the limited number on color palettes, the faint constellations have to share the same color.")
			print("    By default, it is 3.")
			print()
			print("Press enter to continue")
			read()
			
			clearTerminal()
			print("Supported arguments (4/5):")
			print("  -d <Constellation-Name>, --detail <Constellation-Name>")
			print("   Sets the initial constellation whose detailed moon phase information will be shown on the second monitor. By default, it is Horologium.")		
			print()
			print("  -W, --dontWrite")
			print("    Makes the program to not write data to file.")
			print()
			print("  -s, --stop")
			print("    Stops the program after reading data. Useful for resetting multiple data without starting to run the program.")
			print()
			print("Press enter to continue")
			read()
			
			clearTerminal()
			print("Supported arguments (5/5):")
			print("  -h, --help")
			print("    Shows help and stops the program.")
			print()
			print("  -v, --version")
			print("    Shows the current version of this program and stops it.")
			print()
			print("End of help.")
			print()
			return false
		else
			print("Error: Unknown argument. Run again with -h or --help to see supported arguments.")
			return false
		end
		
		readingIdx = readingIdx + 1
	end
	
	return true
end
if not readProgramArgs() then
	return
end


------------------------------------------------------------------------------
-- Step 2. Read stored data, reset data, store data
------------------------------------------------------------------------------

local DIR_NAME = "AS_ConstellationIndicator"
local FILE_NAME = DIR_NAME .. "/data.txt"

local MONITORS = {
	FIRST = "firstMon",
	SECOND = "secondMon"
}

local firstMonSide, secondMonSide = "", ""
local fisrtMon, secondMon = nil, nil
local rednetSide = nil
local solarEclipseDay = 0
local moonPhases = {} -- It will contain the moon phases of all the 16 constellations, including the ones with fixed moon phases.

local function verifyVersion(str)
	if str == nil then
		return { major = nil, minor = nil, patch = nil, error = "No version." }
	end
	local majorStr, minorStr, patchStr = string.match(str, "v(%d+)%.(%d+)%.(%d+)")
	if majorStr == nil or minorStr == nil or patchStr == nil then
		return { major = nil, minor = nil, patch = nil, error = "Unknown version." }
	end
	local major = tonumber(majorStr)
	local minor = tonumber(minorStr)
	local patch = tonumber(patchStr)
	if major == nil or minor == nil or patch == nil then
		return { major = nil, minor = nil, patch = nil, error = "Unknown version." }
	end
	return { major = major, minor = minor, patch = patch, error = nil }
end

local function verifyMonitorSide(str, whichMon)
	if str == nil or str == "" then
		return { value = nil, mon = nil, error = nil } -- No monitor is assigned.
	end
	
	if (whichMon == MONITORS.FIRST and str == secondMonSide) or (whichMon == MONITORS.SECOND and str == firstMonSide) then
		return { value = nil, mon = nil, error = "The monitor at such side or in such network is already in use." }
	end
	
	local mon = peripheral.wrap(str)
	if mon == nil or peripheral.getType(str) ~= "monitor" then
		return { value = nil, mon = nil, error = "Cannot find any monitor at such side or in such network." }
	end
	
	return { value = str, mon = mon, error = nil }
end

local function verifyRednetSide(str)
	if str == nil or str == "" then
		return { value = nil, error = nil } -- No modem is assigned.
	end
	if str ~= 'top' and str ~= 'bottom' and str ~= 'left' and str ~= 'right' and str ~= 'front' and str ~= 'back' then
		return { value = nil, error = "Unexpected value. Expected 'top', 'bottom', 'left', 'right', 'front' or 'back'." }
	end
	if peripheral.getType(str) ~= "modem" then
		return { value = nil, error = "Cannot find any modem at such side." }
	end
	return { value = str, error = nil }
end

local function verifySolarEclipseDay(str)
	if str == nil then
		return { value = nil, error = "Unexpected value. Expected a number." }
	end
	
	local value = tonumber(str)
	if value == nil then
		return { value = nil, error = "Unexpected value. Expected a number but got '" .. str .. "'." }
	end
	
	return { value = value, error = nil }
end

local function verifyMoonPhases(str)
	if str == nil then
		return { value = nil, error = "Unexpected value. Expected an array of 0s and 1s." }
	end
	
	local len = string.len(str)
	if len ~= 8 then
		return { value = nil, error = "Unexpected number of moon phases. Expected 8 but got " .. len .. "." }
	end
	
	local value = {}
	for i = 1, len do
		local char = string.sub(str, i, i)
		local num = tonumber(char)
		if num == nil or num < 0 or num > 1 then
			return { value = nil, error = "Unexpected value. Expected an array of 0s and 1s but got a '" .. char .. "'." }
		end
		table.insert(value, num)
	end
	return { value = value, error = nil }
end

local READ_DATA_FROM_FILE_ERROR = {
	NO_ERROR = 0,
	NO_SUCH_FILE = 1,
	INCONSISTENT_SETUP = 2,
	FATAL_ERROR = 3
}

-- Reads data from existing data file. Returns error code.
local function readDataFromFile()
	if not fs.exists(FILE_NAME) then
		return READ_DATA_FROM_FILE_ERROR.NO_SUCH_FILE
	end
	
	local file = fs.open(FILE_NAME, "r")
	
	-- Version
	-- We may need to compare the data file version against the program version and do something in the future.
	local versionData = verifyVersion(file.readLine())
	if versionData.error ~= nil then
		file.close()
		return READ_DATA_FROM_FILE_ERROR.FATAL_ERROR
	end

	local isSetupError, isFatalError = false, false

	-- First monitor.
	local firstMonSideData = verifyMonitorSide(file.readLine(), MONITORS.FIRST)
	if firstMonSideData.error ~= nil then
		isSetupError = true
	else
		firstMonSide = firstMonSideData.value
		firstMon = firstMonSideData.mon
	end
	
	-- Second monitor.
	local secondMonSideData = verifyMonitorSide(file.readLine(), MONITORS.SECOND)
	if secondMonSideData.error ~= nil then
		isSetupError = true
	else
		secondMonSide = secondMonSideData.value
		secondMon = secondMonSideData.mon
	end

	-- Rednet modem.
	local rednetSideData = verifyRednetSide(file.readLine())
	if rednetSideData.error ~= nil then
		isSetupError = true
	else
		rednetSide = rednetSideData.value
	end
	
	-- Solar eclipse day
	local seData = verifySolarEclipseDay(file.readLine())
	if seData.error ~= nil then
		isFatalError = true
	else
		solarEclipseDay = seData.value
	end
	
	for i = 1, #CONSTELLATIONS do
		local constellation = CONSTELLATIONS[i]
		local name = constellation.name
		local type = constellation.type
		local palette = constellation.palette
		local show = constellation.show
		if show ~= nil then
			table.insert(moonPhases, { name = name, type = type, palette = palette, show = show })
		else
			local moonPhasesData = verifyMoonPhases(file.readLine())
			if moonPhasesData.error ~= nil then
				moonPhases = {}
				isFatalError = true
				break
			else
				table.insert(moonPhases, { name = name, type = type, palette = palette, show = moonPhasesData.value })
			end
		end
	end
	
	file.close()
	if isFatalError then
		return READ_DATA_FROM_FILE_ERROR.FATAL_ERROR
	elseif isSetupError then
		return READ_DATA_FROM_FILE_ERROR.INCONSISTENT_SETUP
	else
		return READ_DATA_FROM_FILE_ERROR.NO_ERROR
	end
end

-- Writes all data into data file.
local function writeDataToFile()
	if not canWriteToFile then
		return
	end
	fs.makeDir(DIR_NAME)
	local file = fs.open(FILE_NAME, "w")
	file.writeLine("v" .. VERSION)
	file.writeLine(firstMonSide)
	file.writeLine(secondMonSide)
	file.writeLine(rednetSide)
	file.writeLine(solarEclipseDay)
	
	for i = 1, #CONSTELLATIONS do
		local constellation = CONSTELLATIONS[i]
		if constellation.show == nil then
			local mp = moonPhases[i]
			file.writeLine(table.concat(mp.show, ""))
		end
	end
	file.close()
end


local function readMonitorSideFromInput(step, whichSide)
	if step == nil then
		step = ""
	end
	local sideDesc = ""
	if whichSide == MONITORS.FIRST then
		sideDesc = "first monitor. It will be used for showing the constellation visibility chart."
	elseif whichSide == MONITORS.SECOND then
		sideDesc = "second monitor. It will be used for showing the detailed information of one specific constellation at a time."
	end
	print(step .. "Please enter the side to the " .. sideDesc .. " Expected values are: 'top', 'bottom', 'left', 'right', 'front', 'back' or network name.")
	print("Leave it empty if no monitor will be used for this purpose.")

	local noError = true
	repeat
		noError = true
		local sideData = verifyMonitorSide(read(), whichSide)
		if sideData.error ~= nil then
			print("Error: " .. sideData.error .. " Please enter again.")
			noError = false
		else
			if whichSide == MONITORS.FIRST then
				firstMonSide = sideData.value
				firstMon = sideData.mon
			elseif whichSide == MONITORS.SECOND then
				secondMonSide = sideData.value
				secondMon = sideData.mon
			end
		end
	until noError
end

local function readRednetSideFromInput(step)
	if step == nil then
		step = ""
	end
	print(step .. "Please enter the side to the modem for sending Rednet messages. Expected values are: 'top', 'bottom', 'left', 'right', 'front' or 'back'.")
	print("Leave it empty if no modem will be used for this purpose.")

	local noError = true
	repeat
		noError = true
		local sideData = verifyRednetSide(read())
		if sideData.error ~= nil then
			print("Error: " .. sideData.error .. " Please enter again.")
			noError = false
		else
			rednetSide = sideData.value
		end
	until noError
end

local function readSolarEclipseDayFromInput(step)
	if step == nil then
		step = ""
	end
	print(step .. "Please enter the day number when there was a Solar Eclipse.")
	
	local noError = true
	repeat
		noError = true
		local seData = verifySolarEclipseDay(read())
		if seData.error ~= nil then
			print("Error: " .. seData.error .. " Please enter again.")
			noError = false
		else
			solarEclipseDay = seData.value
		end
	until noError
end

local function readConstellationMoonPhasesFromInput(constellationIndex, constellationStep, maxConstellationStep)
	local constellation = CONSTELLATIONS[constellationIndex]
	local name = constellation.name
	if constellationStep ~= nil then
		print(name .. " (" .. tostring(constellationStep) .. "/" .. tostring(maxConstellationStep) .. ")")
	else
		print("Please enter the visibilities of " .. name .. " during the 8 moon phases with 0 (absence) and 1 (visible).")
	end
	
	local noError = true
	repeat
		noError = true
		local moonPhasesData = verifyMoonPhases(read())
		if moonPhasesData.error ~= nil then
			print("Error: " .. moonPhasesData.error .. " Please enter again.")
			noError = false
		else
			moonPhases[constellationIndex].show = moonPhasesData.value
		end
	until noError
end

local function readDataFromInput()
	readMonitorSideFromInput("1. ", MONITORS.FIRST)
	print()
	readMonitorSideFromInput("2. ", MONITORS.SECOND)
	print()
	readRednetSideFromInput("3. ")
	print()
	readSolarEclipseDayFromInput("4. ")
	print()
	print("5. Please enter the visibilities of each constellation (except Horologium and Pelotrio) during the 8 moon phases with 0 (absence) and 1 (visible).")
	print("For example, for a constellation that appears on the first and the last four moon phases, enter '10001111'")
	print("Enter eight 0s if you have not known the visibilities of the constellation. You can always update it by relaunching the program with the -r argument.")

	local step = 1
	for i, constellation in ipairs(CONSTELLATIONS) do
		local name = constellation.name
		local type = constellation.type
		local palette = constellation.palette
		local show = constellation.show
		if show ~= nil then
			table.insert(moonPhases, { name = name, type = type, palette = palette, show = show })
		else
			table.insert(moonPhases, { name = name, type = type, palette = palette, show = "" })
			readConstellationMoonPhasesFromInput(i, step, numDynamicConstellations)
			step = step + 1
		end
	end
end



clearTerminal()

term.setTextColor(colors.yellow)
print("Astral Sorcery - Constellation Indicator v" .. VERSION)
term.setTextColor(colors.white)
print()

-- Actually try to read data from file.
if resetType == RESET_TYPE.ALL then
	-- But reset all data. No need to read.
	print("Reset all data.")
	readDataFromInput()
	writeDataToFile()
else
	local code = readDataFromFile()
	if code == READ_DATA_FROM_FILE_ERROR.NO_SUCH_FILE then
		print("No saved data is found. Please enter the necessary data one by one.")
		print()
		readDataFromInput()
		writeDataToFile()
	elseif code == READ_DATA_FROM_FILE_ERROR.FATAL_ERROR then
		print("Unable to read data from file. Please enter the necessary data one by one.")
		print()
		readDataFromInput()
		writeDataToFile()
	else
		local needWriteToFile = false
		if code == READ_DATA_FROM_FILE_ERROR.INCONSISTENT_SETUP then
			print("Successfully read data from file, but its setup is inconsistent with the current setup. Please enter the necessary data one by one.")
			print()
			readMonitorSideFromInput("1. ", MONITORS.FIRST)
			print()
			readMonitorSideFromInput("2. ", MONITORS.SECOND)
			print()
			readRednetSideFromInput("3. ")
			print()
			needWriteToFile = true
		elseif code == READ_DATA_FROM_FILE_ERROR.NO_ERROR then
			print("Successfully read data from file.")
		end

		local function readMultipleConstellationsMoonPhasesFromInput(type)
			if type == nil then
				local step = 1
				for i, constellation in ipairs(CONSTELLATIONS) do
					if constellation.show == nil then
						readConstellationMoonPhasesFromInput(i, step, numDynamicConstellations)
						step = step + 1
					end
				end
			else
				local numSteps = 0
				for _, constellation in ipairs(CONSTELLATIONS) do
					if constellation.type == type and constellation.show == nil then
						numSteps = numSteps + 1
					end
				end
				local step = 1
				for i, constellation in ipairs(CONSTELLATIONS) do
					if constellation.type == type and constellation.show == nil then
						readConstellationMoonPhasesFromInput(i, step, numSteps)
						step = step + 1
					end
				end
			end
		end

		if resetType == RESET_TYPE.FIRST_MON and code ~= READ_DATA_FROM_FILE_ERROR.INCONSISTENT_SETUP then
			print("Reset the side to the first monitor.")
			print()
			readMonitorSideFromInput(nil, MONITORS.FIRST)
			needWriteToFile = true
		elseif resetType == RESET_TYPE.SECOND_MON and code ~= READ_DATA_FROM_FILE_ERROR.INCONSISTENT_SETUP then
			print("Reset the side to the second monitor.")
			print()
			readMonitorSideFromInput(nil, MONITORS.SECOND)
			needWriteToFile = true
		elseif resetType == RESET_TYPE.REDNET and code ~= READ_DATA_FROM_FILE_ERROR.INCONSISTENT_SETUP then
			print("Reset the side to the modem for sending Rednet messages.")
			print()
			readRednetSideFromInput(nil)
			needWriteToFile = true
		elseif resetType == RESET_TYPE.SOLAR_ECLIPSE then
			print("Reset the solar eclipse day.")
			print()
			readSolarEclipseDayFromInput(nil)
			needWriteToFile = true
		elseif resetType == RESET_TYPE.CONSTELLATION_ONE then
			print("Reset the moon phases of " .. CONSTELLATIONS[resetConstellationIndex].name .. ".")
			print()
			readConstellationMoonPhasesFromInput(resetConstellationIndex, nil, nil)
			needWriteToFile = true
		elseif resetType == RESET_TYPE.CONSTELLATION_BRIGHT then
			print("Reset the moon phases of all bright constellations.")
			print()
			print("Please enter the visibilities of each bright constellation during the 8 moon phases with 0 (absence) and 1 (visible).")
			readMultipleConstellationsMoonPhasesFromInput(1)
			needWriteToFile = true
		elseif resetType == RESET_TYPE.CONSTELLATION_DIM then
			print("Reset the moon phases of all dim constellations.")
			print()
			print("Please enter the visibilities of each dim constellation during the 8 moon phases with 0 (absence) and 1 (visible).")
			readMultipleConstellationsMoonPhasesFromInput(2)
			needWriteToFile = true
		elseif resetType == RESET_TYPE.CONSTELLATION_FAINT then
			print("Reset the moon phases of all faint constellations.")
			print()
			print("Please enter the visibilities of each faint constellation during the 8 moon phases with 0 (absence) and 1 (visible).")
			readMultipleConstellationsMoonPhasesFromInput(3)
			needWriteToFile = true
		elseif resetType == RESET_TYPE.CONSTELLATION_ALL then
			print("Reset the moon phases of all constellations.")
			print()
			print("Please enter the visibilities of each constellation during the 8 moon phases with 0 (absence) and 1 (visible).")
			readMultipleConstellationsMoonPhasesFromInput(nil)
			needWriteToFile = true
		end
		if needWriteToFile then
			writeDataToFile()
		end
	end
end

print()

if not canRun then
	print("All data received. But -s is detected. Program stopped.")
	return
end

------------------------------------------------------------------------------
-- Step 3. Prepare the monitors
------------------------------------------------------------------------------

if firstMonSide ~= nil or secondMonSide ~= nil then
	print("Prepare the monitors...")
	print()
	sleep(1)
end

-- Returns the moon phase on the given day. If the day is not get by os.day() but instead a solar eclipse day, it should be added by one.
local function getMoonPhaseByDayNum(day)
	return (day / 8) % 1 * 8 + 1
end


-- The first monitor has 3 color schemes:
-- 1 = All white
-- 2 = Colored by constellation type
-- 3 = Colored by constellation color

local CONSTELLATION_TYPE_COLORS = {
	{ palette = colors.orange, color = 0x2843CC },
	{ palette = colors.magenta, color = 0x432CB0 },
	{ palette = colors.lightBlue, color = 0x5D197F }
}

local CONSTELLATION_COLORS = {
	{ palette = colors.orange, color = 0x2EE400 },
	{ palette = colors.magenta, color = 0xB7BBB8 },
	{ palette = colors.lightBlue, color = 0xE01903 },
	{ palette = colors.yellow, color = 0xA00100 },
	{ palette = colors.pink, color = 0x00BDAD },
	{ palette = colors.gray, color = 0xD41CD6 },
	{ palette = colors.lightGray, color = 0xFF4E1B },
	{ palette = colors.cyan, color = 0x7D16B4 },
	{ palette = colors.purple, color = 0xFFE709 },
	{ palette = colors.blue, color = 0xCB7D0A },
	{ palette = colors.brown, color = 0x706EFF },
	{ palette = colors.green, color = 0xEC006B },
	{ palette = colors.red, color = 0x5D197F }
}

-------------------- MonHelperBase --------------------
local MonHelperBase = {}
MonHelperBase.__index = MonHelperBase
setmetatable(MonHelperBase, {
	__call = function(cls, ...)
		local self = setmetatable({}, cls)
		self:_init(...)
		return self
	end
})

function MonHelperBase:_init(monitor)
	self.monitor = monitor
	self.sizeX, self.sizeY = monitor.getSize()
	self.cursorStartX = 2
	self.cursorStartY = 1

	monitor.clear()
	monitor.setPaletteColor(colors.black, 0x111111)
	monitor.setPaletteColor(colors.white, 0xF0F0F0)
	monitor.setBackgroundColor(colors.black)
end

-------------------- FirstMonHelper --------------------
local FirstMonHelper = {}
FirstMonHelper.__index = FirstMonHelper
setmetatable(FirstMonHelper, {
	__index = MonHelperBase,
	__call = function(cls, ...)
		local self = setmetatable({}, cls)
		self:_init(...)
		return self
	end
})

function FirstMonHelper:_init(monitor)
	MonHelperBase._init(self, monitor)

	monitor.setCursorPos(self.cursorStartX + 13, self.cursorStartY)
	monitor.setTextColor(colors.white)
	monitor.write("Moon Phase")

	monitor.setPaletteColor(colors.lime, 0x7FCC19)
	if initialColorScheme == nil then
		self.colorScheme = 3
	else
		self.colorScheme = initialColorScheme
	end
	self:updatePalettesColors()
	self:drawConstellationNames()
end

function FirstMonHelper:drawConstellationNames()
	for i, mp in pairs(moonPhases) do
		self.monitor.setCursorPos(self.cursorStartX, self.cursorStartY + 1 + i)

		if self.colorScheme == 1 then
			self.monitor.setTextColor(colors.white)
		elseif self.colorScheme == 2 then
			self.monitor.setTextColor(CONSTELLATION_TYPE_COLORS[mp.type].palette)
		elseif self.colorScheme == 3 then
			self.monitor.setTextColor(mp.palette)
		end

		self.monitor.write(mp.name)
	end
end

function FirstMonHelper:updatePalettesColors()
	if self.colorScheme == 2 then
		for _, val in pairs(CONSTELLATION_TYPE_COLORS) do
			self.monitor.setPaletteColor(val.palette, val.color)
		end
	elseif self.colorScheme == 3 then
		for _, val in pairs(CONSTELLATION_COLORS) do
			self.monitor.setPaletteColor(val.palette, val.color)
		end
	end
end

function FirstMonHelper:changeColorScheme()
	self.colorScheme = self.colorScheme + 1
	if self.colorScheme > 3 then
		self.colorScheme = 1
	end
	self:updatePalettesColors()
	self:drawConstellationNames()
end

function FirstMonHelper:drawMoonPhases(currentDay, currentMoonPhase, nextSolarEclipse)
	for x = 1, 8 do
		local phase = currentMoonPhase - 1 + x
		if phase > 8 then
			phase = phase - 8
		end
		if x == 1 then
			self.monitor.setTextColor(colors.lime)
		else
			self.monitor.setTextColor(colors.white)
		end
		self.monitor.setCursorPos(self.cursorStartX + 9 + 2 * x, self.cursorStartY + 1)
		self.monitor.write(tostring(phase))
		
		for y, mp in ipairs(moonPhases) do
			self.monitor.setCursorPos(self.cursorStartX + 9 + 2 * x, self.cursorStartY + 1 + y)
			local show = mp.show[phase]
			if show == 0 then
				self.monitor.write(" ")
			elseif show == 1 then
				self.monitor.write("X")
			elseif show == 2 then
				if nextSolarEclipse == currentDay - 1 + x then
					self.monitor.write("X")
				else
					self.monitor.write(" ")
				end
			end
		end
	end
end

-------------------- SecondMonHelper --------------------

local SecondMonHelper = {}
SecondMonHelper.__index = SecondMonHelper
setmetatable(SecondMonHelper, {
	__index = MonHelperBase,
	__call = function(cls, ...)
		local self = setmetatable({}, cls)
		self:_init(...)
		return self
	end
})

function SecondMonHelper:_init(monitor)
	MonHelperBase._init(self, monitor)

	monitor.setPaletteColor(colors.gray, 0x808080)
	monitor.setPaletteColor(colors.yellow, 0xFFFF00)
	for _, val in pairs(CONSTELLATION_TYPE_COLORS) do
		monitor.setPaletteColor(val.palette, val.color)
	end

	monitor.setCursorPos(self.cursorStartX, self.cursorStartY)
	monitor.setTextColor(colors.white)
	monitor.write("Detail info of ")

	monitor.setCursorPos(self.cursorStartX, self.cursorStartY + 2)
	monitor.write("Moon phases:")

	monitor.setCursorPos(self.cursorStartX, self.cursorStartY + 5)
	monitor.write("Next visible day:")

	monitor.setCursorPos(self.cursorStartX + 29, self.cursorStartY + 5)
	monitor.write("And then on:")

	monitor.setCursorPos(self.cursorStartX, self.cursorStartY + 8)
	monitor.write("Together with:")

	monitor.setCursorPos(self.cursorStartX + 29, self.cursorStartY + 8)
	monitor.write("Together with:")

	self.constellationIndex = 1
	if initialDetailedConstellationIndex == nil then
		local done = false
		for i, mp in ipairs(moonPhases) do
			for _, show in pairs(mp.show) do
				if show == 2 then
					self.constellationIndex = i
					done = true
					break
				end
			end
			if done then
				break
			end
		end
	else
		self.constellationIndex = initialDetailedConstellationIndex
	end
	self:drawConstellationStaticInfo()
end

function SecondMonHelper:drawConstellationStaticInfo()
	local mp = moonPhases[self.constellationIndex]

	-- Match the last palette in CONSTELLATION_COLORS to be the desired constellation color so we can free the other palettes.
	local color = 0
	for _, val in ipairs(CONSTELLATION_COLORS) do
		if val.palette == mp.palette then
			color = val.color
			break
		end
	end
	local palette = CONSTELLATION_COLORS[#CONSTELLATION_COLORS].palette
	self.monitor.setPaletteColor(palette, color)

	self.monitor.setCursorPos(self.cursorStartX + 15, self.cursorStartY)
	self.monitor.write("                    ")
	self.monitor.setCursorPos(self.cursorStartX + 15, self.cursorStartY)
	self.monitor.setTextColor(palette)
	self.monitor.write(mp.name)

	self.monitor.setCursorPos(self.cursorStartX + 1, self.cursorStartY + 3)
	self.monitor.clearLine()

	local isSolarEclipse = false
	for i, show in ipairs(mp.show) do
		if show == 0 then
			self.monitor.setCursorPos(self.cursorStartX + i * 2 - 1, self.cursorStartY + 3)
			self.monitor.setTextColor(colors.gray)
			self.monitor.write(tostring(i))
		elseif show == 1 then
			self.monitor.setCursorPos(self.cursorStartX + i * 2 - 1, self.cursorStartY + 3)
			self.monitor.setTextColor(colors.yellow)
			self.monitor.write(tostring(i))
		elseif show == 2 then
			self.monitor.setCursorPos(self.cursorStartX + 1, self.cursorStartY + 3)
			self.monitor.clearLine()
			self.monitor.setTextColor(colors.lime)
			self.monitor.write("(The night after solar eclipse)")
			isSolarEclipse = true
			break
		end
	end

	if not isSolarEclipse then
		if CONSTELLATIONS[self.constellationIndex].show ~= nil then
			self.monitor.setTextColor(colors.lime)
			self.monitor.write(" (Fixed in every world)")
		end
	end
end

function SecondMonHelper:drawConstellationDynamicInfo(currentDay, currentMoonPhase, nextSolarEclipse)
	local mp = moonPhases[self.constellationIndex]

	function getNextVisibleDayAndMoonPhase(startDay, startMoonPhase)
		local done = false
		repeat
			local show = mp.show[startMoonPhase]
			if show == 0 then
				startDay = startDay + 1
				startMoonPhase = getMoonPhaseByDayNum(startDay)
			elseif show == 1 then
				done = true
			elseif show == 2 then
				done = true
				if startDay > nextSolarEclipse then
					startDay = nextSolarEclipse + 36
				else
					startDay = nextSolarEclipse
				end
				startMoonPhase = getMoonPhaseByDayNum(startDay)
			end
		until done
		return startDay, startMoonPhase
	end

	function drawNextVisibleDayAndOtherConstellations(cursorStartX, cursorStartY, visibleDay, visibleMoonPhase)
		self.monitor.setCursorPos(cursorStartX, cursorStartY)

		if visibleDay == currentDay then
			self.monitor.setTextColor(colors.yellow)
			self.monitor.write("Tonight!")
		else
			self.monitor.setTextColor(colors.white)
			self.monitor.write("Day " .. tostring(visibleDay + 1))
		end

		local step = 0
		for i, mp in ipairs(moonPhases) do
			if i ~= self.constellationIndex then
				local show = mp.show[visibleMoonPhase]
				if show == 1 or (show == 2 and (visibleDay - solarEclipseDay) % 36 == 0) then
					self.monitor.setCursorPos(cursorStartX + (step % 2) * 12, cursorStartY + 3 + math.floor(step / 2))
					self.monitor.setTextColor(CONSTELLATION_TYPE_COLORS[mp.type].palette)
					self.monitor.write(mp.name)
					step = step + 1
				end
			end
		end
	end

	self.monitor.setCursorPos(self.cursorStartX, self.cursorStartY + 6)
	self.monitor.clearLine()
	for i = 1, 7 do
		self.monitor.setCursorPos(self.cursorStartX, self.cursorStartY + 9 + i - 1)
		self.monitor.clearLine()
	end

	local nextVisibleDay, nextVisibleMoonPhase = getNextVisibleDayAndMoonPhase(currentDay, currentMoonPhase)
	drawNextVisibleDayAndOtherConstellations(self.cursorStartX + 1, self.cursorStartY + 6, nextVisibleDay, nextVisibleMoonPhase)

	nextVisibleDay, nextVisibleMoonPhase = getNextVisibleDayAndMoonPhase(nextVisibleDay + 1, getMoonPhaseByDayNum(nextVisibleDay + 1))
	drawNextVisibleDayAndOtherConstellations(self.cursorStartX + 30, self.cursorStartY + 6, nextVisibleDay, nextVisibleMoonPhase)
end

function SecondMonHelper:changeConstellation(currentDay, currentMoonPhase, nextSolarEclipse)
	self.constellationIndex = self.constellationIndex + 1
	if self.constellationIndex > #moonPhases then
		self.constellationIndex = 1
	end
	self:drawConstellationStaticInfo()
	self:drawConstellationDynamicInfo(currentDay, currentMoonPhase, nextSolarEclipse)
end

------------------------------------------------------------------------------
-- Step 4. Run and loop
------------------------------------------------------------------------------

if rednetSide ~= nil then
	print("Open modem for Rednet...")
	print()
	rednet.open(rednetSide)
	sleep(1)
end

local firstMonHelper, secondMonHelper
if firstMon ~= nil then
	firstMonHelper = FirstMonHelper(firstMon)
end
if secondMon ~= nil then
	secondMonHelper = SecondMonHelper(secondMon)
end

print("Preparation completed. Enjoy.")
print()

local currentDay, currentMoonPhase, oldMoonPhase, nextSolarEclipse = 0, 0, 0, 0

-- currentDay will be minus one, so we need to do the same to solarEclipseDay
solarEclipseDay = solarEclipseDay - 1

local running = true
local activeConstellations = nil

local function mainLoop()
	currentDay = os.day() - 1
	if os.time() < 6 then
		currentDay = currentDay - 1
	end
	currentMoonPhase = getMoonPhaseByDayNum(currentDay)
	nextSolarEclipse = math.floor((currentDay - solarEclipseDay) / 36) * 36 + solarEclipseDay
	if nextSolarEclipse < currentDay then
		nextSolarEclipse = nextSolarEclipse + 36
	end
	
	if oldMoonPhase ~= currentMoonPhase then
		if firstMonHelper ~= nil then
			firstMonHelper:drawMoonPhases(currentDay, currentMoonPhase, nextSolarEclipse)
		end
		if secondMonHelper ~= nil then
			secondMonHelper:drawConstellationDynamicInfo(currentDay, currentMoonPhase, nextSolarEclipse)
		end
		if rednetSide ~= nil then
			activeConstellations = ""
			for _, mp in ipairs(moonPhases) do
				local show = mp.show[currentMoonPhase]
				if show == 0 then
					activeConstellations = activeConstellations .. "0"
				elseif show == 1 then
					activeConstellations = activeConstellations .. "1"
				elseif show == 2 then
					if nextSolarEclipse == currentDay then
						activeConstellations = activeConstellations .. "1"
					else
						activeConstellations = activeConstellations .. "0"
					end
				end
			end
			rednet.broadcast(activeConstellations, REDNET_PROTOCOLS.BROADCAST_CONSTELLATIONS)
		end
	end
	
	oldMoonPhase = currentMoonPhase
	sleep(10)
end

local function onEvent()
	local event, id, message, protocol = os.pullEventRaw()
	if event == "terminate" then
		running = false
		return
	elseif event == "monitor_touch" then
		if id == firstMonSide and firstMonHelper ~= nil then
			firstMonHelper:changeColorScheme()
		elseif id == secondMonSide and secondMonHelper ~= nil then
			secondMonHelper:changeConstellation(currentDay, currentMoonPhase, nextSolarEclipse)
		end
	elseif event == "monitor_resize" or event == "peripheral" then
		local type = peripheral.getType(id)
		if id == firstMonSide and type == "monitor" and firstMonHelper ~= nil then
			firstMonHelper = FirstMonHelper(peripheral.wrap(firstMonSide))
			firstMonHelper:drawMoonPhases(currentDay, currentMoonPhase, nextSolarEclipse)
		elseif id == secondMonSide and type == "monitor" and secondMonHelper ~= nil then
			secondMonHelper = SecondMonHelper(peripheral.wrap(secondMonSide))
			secondMonHelper:drawConstellationDynamicInfo(currentDay, currentMoonPhase, nextSolarEclipse)
		elseif id == rednetSide and type == "modem" then
			rednet.open(rednetSide)
			rednet.broadcast(activeConstellations, REDNET_PROTOCOLS.BROADCAST_CONSTELLATIONS)
		end
	elseif event == "rednet_message" then
		if protocol == REDNET_PROTOCOLS.REQUEST_CONSTELLATION then
			local sender = id
			local constellationIndex = tonumber(message)
			local response
			if activeConstellations == nil then
				response = "0"
			else
				response = string.sub(activeConstellations, constellationIndex, constellationIndex)
			end
			print("Constellation status request received. Request: " .. message .. " (" .. CONSTELLATIONS[constellationIndex].name .. "). Response: " .. response)
			print()
			rednet.send(sender, response, REDNET_PROTOCOLS.RESPONSE_CONSTELLATION)
		end
	end
end

while running do
	parallel.waitForAny(onEvent, mainLoop)
end

print("Terminating...")
print()
sleep(1)
if firstMon ~= nil then
	firstMon.clear()
end
if secondMon ~= nil then
	secondMon.clear()
end
if rednetSide ~= nil then
	print("Close modem for Rednet...")
	if peripheral.getType(rednetSide) == "modem" then
		local message = ""
		for i = 1, #moonPhases do
			message = message .. "0"
		end
		rednet.broadcast(message, REDNET_PROTOCOLS.BROADCAST_CONSTELLATIONS)
		rednet.close(rednetSide)
	else
		print("But modem no longer exists.")
	end
	print()
end
print("Terminated. Good bye!")
print()