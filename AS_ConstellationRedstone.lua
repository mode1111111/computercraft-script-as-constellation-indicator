--License: MIT

local VERSION = "1.1.0"

-- A little program to listen Rednet message about the visible constellations and emit redstone signal accordingly.
local CONSTELLATION_NAMES = {
	"Aevitas", "Armara", "Discidia", "Evorsio", "Vicio",
	"Bootes", "Fornax", "Horologium", "Lucerna", "Mineralis", "Octans", "Pelotrio",
	"Alcara", "Gelu", "Ulteria", "Vorux"
}

local REDNET_PROTOCOLS = {
	BROADCAST_CONSTELLATIONS = "AS_ConstellationIndicator_Broadcast",
	REQUEST_CONSTELLATION = "AS_ConstellationIndicator_Request",
	RESPONSE_CONSTELLATION = "AS_ConstellationIndicator_Response"
}

local tArgs = {...}

if tArgs == nil or #tArgs < 3 then
	print("Please set the side connecting to the modem for receiving Rednet messages, the side for emitting redstone signal, and the constellation name.")
	print("Usage: <program> <modem-side> <redstone-side> <Constellation-Name>")
	print()
	return
end

local modemSide = tArgs[1]
local redstoneSide = tArgs[2]
local constellation = tArgs[3]

local function isStringASide(str)
	return str == 'top' or str == 'bottom' or str == 'left' or str == 'right' or str == 'front' or str == 'back'
end

-- Verify modemSide
if not isStringASide(modemSide) then
	print("Error: Unexpected value for modem side. Expected 'top', 'bottom', 'left', 'right', 'front' or 'back'.")
	return
end
if peripheral.getType(modemSide) ~= "modem" then
	print("Error: Cannot find modem at " .. modemSide .. " side.")
	return
end

-- Verify redstoneSide
if not isStringASide(redstoneSide) then
	print("Error: Unexpected value for redstone side. Expected 'top', 'bottom', 'left', 'right', 'front' or 'back'.")
	return
end

-- Verify constellation
local constellationIndex = -1
for i, name in ipairs(CONSTELLATION_NAMES) do
	if name == constellation then
		constellationIndex = i
	end
end
if constellationIndex <= -1 then
	print("Error: Unrecognized constellation name. Please make sure it is a valid name with the first letter in uppercase.")
	return
end

term.clear()
term.setCursorPos(1, 1)
term.setTextColor(colors.yellow)
print("Astral Sorcery - Constellation Redstone v" .. VERSION)
term.setTextColor(colors.white)
print()
print("Program started.")
print()
print("Modem side: " .. modemSide)
print("Redstone side: " .. redstoneSide)
print("Constellation: " .. constellation)
print()

rednet.open(modemSide)

local running = true
local function onEvent()
	local event, id, message, protocol = os.pullEventRaw()
	if event == "terminate" then
		running = false
		return
	elseif event == "rednet_message" then
		if protocol == REDNET_PROTOCOLS.BROADCAST_CONSTELLATIONS then
--			print("Message: " .. message .. ", SubStr: " .. string.sub(message, constellationIndex, constellationIndex))
			local isOn = string.sub(message, constellationIndex, constellationIndex) == "1"
			redstone.setOutput(redstoneSide, isOn)
		elseif protocol == REDNET_PROTOCOLS.RESPONSE_CONSTELLATION then
--			print("Response Message: " .. message)
			local isOn = message == "1"
			redstone.setOutput(redstoneSide, isOn)
		end
	elseif event == "peripheral" then
		if id == modemSide and peripheral.getType(id) == "modem" then
			rednet.open(modemSide)
			rednet.broadcast(tostring(constellationIndex), REDNET_PROTOCOLS.REQUEST_CONSTELLATION)
		end
	end
end

rednet.broadcast(tostring(constellationIndex), REDNET_PROTOCOLS.REQUEST_CONSTELLATION)

while running do
	onEvent()
end

print("Terminating...")
print()

redstone.setOutput(redstoneSide, false)
if peripheral.getType(modemSide) == "modem" then
	rednet.close(modemSide)
end

print("Terminated. Good bye!")
print()