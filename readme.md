# ComputerCraft - AS Constellation Indicator

This repository contains some scripts that can be used by the computers in ComputerCraft of Minecraft 1.16. The purpose of these scripts is to provide information about the days when the Astral Sorcery constellations become visible and sand redstone signals accordingly.

## Features
- A chart of moon phases that shows the visibilities of all constellations in the following 8 days.
- Detailed information of constellation that shows 2 day numbers when it will be visible, and also the constellations that will also be visible on these days.
- With slave computers, it forms a system that can use redstone signal to indicate the current visibility of each constellation. They use Rednet to transfer messages.

## How to use
### Moon phase chart and constellation detail info.
1. Make an Advanced Computer and install `AS_ConstellationIndicator.lua` into it.
2. Build a 3 width x 3 height Advanced Monitor and connect to the computer. It will be used for displaying the chart of moon phases.
3. Build another 6 width x 3 height Advanced Monitor and connect to the computer. It will be used for displaying the detail information of one constellation at a time.
4. Run `AS_ConstellationIndicator` in the computer. Run `AS_ConstellationIndicator -h` for more information.

### Redstone signal
1. To easy share the same script across different computers, make a Disk Driver and insert a Disk into it. Copy or move `AS_ConstellationRedstone.lua` into the disk.
2. Build 2 Modems and place next to the Advanced Computer that is running `AS_ConstellationIndicator` and the Disk Driver. This computer will be the master of the system. It will send Rednet messages about the current visibility of each constellation.
3. Build Computers or Advanced Computers and connect to the Advanced Computer and the Disk Driver using Modems. These computers will be the slaves of the system. They will receive Rednet messages from the master and set redstone signal output according to the current visibility of the constellations they are interested at. One slave computer can only be interested at one constellation.
4. In each slave computer, run `disk/AS_ConstellationRedstone <modem-side> <redstone-side> <Constellation-Name>`.

### Startup program
By default, computers will shut down and reboot when their chunks are unloaded. This cause the computers lose their programs when no player is around or server restart. This is especially annoying in single-player world. You can tell the computers to automatically run program(s) again by making a `startup` program.
1. In shell, run `edit startup`.
2. Inside the startup program, write `os.run({}, "<program.lua>"[, "<argument1>", "<arugment2>", ..."<arugmentN>"])`. For example, `os.run({}, "disk/AS_ConstellationRedstone.lua", "back", "top", "Aevitas")`.
3. Save and exit. Now the computer will automatically run the assigned program after reboot.

### Terminate
Hold `CTRL+T` for 3 seconds to terminate the program.

## Credit
Jane from the Astral Sorcery Discord who shared the initial moon phases chart script.

## License
MIT